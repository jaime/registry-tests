# [1.1.0](https://gitlab.com/jaime/registry-tests/compare/v1.0.1-gitlab...v1.1.0-gitlab) (2023-07-20)


### Features

* new lines added ([932357c](https://gitlab.com/jaime/registry-tests/commit/932357c854b020a089e1ea964543cd0a8fcad539))

## [1.0.1](https://gitlab.com/jaime/registry-tests/compare/v1.0.0-gitlab...v1.0.1-gitlab) (2023-07-20)


### Bug Fixes

* problem with thingy ([8389623](https://gitlab.com/jaime/registry-tests/commit/8389623b78debcfff63366c59a274bb70f1af23f))

# 1.0.0 (2023-07-20)


### Bug Fixes

* only ls files ([efca855](https://gitlab.com/jaime/registry-tests/commit/efca8555fdebb1844ccfae933eef664df3082900))


### Features

* hello world ([83d3ef7](https://gitlab.com/jaime/registry-tests/commit/83d3ef78854d77464b7bf9d45f24a22be94943f0))
* new dockerfile tests ([3a94efa](https://gitlab.com/jaime/registry-tests/commit/3a94efaaf98e127b4b4e889985ac7fef2413a05a))

# 1.0.0 (2023-07-20)


### Bug Fixes

* only ls files ([efca855](https://gitlab.com/jaime/registry-tests/commit/efca8555fdebb1844ccfae933eef664df3082900))


### Features

* hello world ([83d3ef7](https://gitlab.com/jaime/registry-tests/commit/83d3ef78854d77464b7bf9d45f24a22be94943f0))
