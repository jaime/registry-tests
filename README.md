# registry-tests

Testing container registry

## Building Multi Arch images on MacOS with Colima

```shell
# install buildx
brew install docker-buildx
# Follow the caveats mentioned in the install instructions:
ln -s $(which docker-buildx) ~/.docker/cli-plugins/docker-buildx

# you need a `buildx.toml` file to define an insecure registry
docker buildx create --name superinsecure --use --config buildx.toml

# build and push an image from a Dockerfile using buildx

docker buildx build --tag registry.test:5000/registry-tests/alpine/multi:latest --platform linux/arm,linux/amd64 --builder superinsecure  --push .
```
