.PHONY: dev-tools

dev-tools:
	@npm install -g \
		@commitlint/cli@17 \
		@commitlint/config-conventional@17 \
		semantic-release@19 \
		@semantic-release/commit-analyzer@9 \
		@semantic-release/release-notes-generator@10 \
		@semantic-release/changelog@6 \
		@semantic-release/git@10
