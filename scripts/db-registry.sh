#!/bin/sh

set -e


DB_USER="${REGISTRY_DB_USER:=registry}"
DB_PASSWORD="${REGISTRY_DB_PASSWORD:=registrysecurepassword}"
DB_NAME="${REGISTRY_DB_NAME:=experimental_registry}"

prompt_confirm() {
  while true; do
    read -r -n 1 -p "${1:-Continue?} [y/n]: " REPLY
    case $REPLY in
      [yY]) echo ; return 0 ;;
      [nN]) echo ; return 1 ;;
      *) printf " \033[31m %s \n\033[0m" "invalid input"
    esac 
  done  
}


restore_all_files () {
	prompt_confirm "restore files to original state?" || exit 1

	echo "restoring files"
	cp /etc/gitlab/gitlab.rb.backup /etc/gitlab/gitlab.rb
	cp  /var/opt/gitlab/registry/config.yml.backup /var/opt/gitlab/registry/config.yml 

	gitlab-ctl stop registry
	echo "restoring database"
  gitlab-psql -c "DROP DATABASE $DB_NAME"
	gitlab-psql -c "DROP USER $DB_USER"

	gitlab-ctl reconfigure
	gitlab-ctl restart postgresql
	gitlab-ctl start registry
}

run_as_root () {
  if [ "$EUID" -ne 0 ]
	then
    echo "Please run as root"
    exit
  fi
}

prepare_gitlab_configuration () {
	prompt_confirm "You are about to configure the metadata database for the registry, do you want to continue?"

  # create a gitlab.rb backaup
  cp /etc/gitlab/gitlab.rb /etc/gitlab/gitlab.rb.backup

  # create a database user and a new logical database
  gitlab-psql -c "CREATE USER $DB_USER WITH PASSWORD '$DB_PASSWORD'"
  gitlab-psql -c "CREATE DATABASE $DB_NAME WITH OWNER $DB_USER"

  tee -a /etc/gitlab/gitlab.rb <<-EOF
postgresql['custom_pg_hba_entries'] = {
  registry_db: [
  {
    type: 'local',
    database: '$DB_NAME',
    user: '$DB_USER',
    method: 'md5'
  }
  ]
}
EOF

	# reconfigure GitLab and restart postgresql
	gitlab-ctl reconfigure
	gitlab-ctl restart postgresql

	# allow postgres to finish restarting
	sleep 5
	echo 'done configuring gitlab'
}


configure_registry_settings () {
	echo 'modifying registry config'

	# verify the registry is running
	echo "run \"gitlab-ctl status registry\" and verify if the registry is running"
	echo " if it's not, follow https://docs.gitlab.com/ee/administration/packages/container_registry.html#linux-package-installations"

	prompt_confirm "Is the registry running?" || restore_all_files

	# stop registry and backup configuration
	gitlab-ctl stop registry
	cp /var/opt/gitlab/registry/config.yml  /var/opt/gitlab/registry/config.yml.backup

	# add the database configuration

	tee -a /var/opt/gitlab/registry/config.yml <<-EOF
database:
  enabled: true
  host: "/var/opt/gitlab/postgresql/"
  user: "$DB_USER"
  password: "$DB_PASSWORD"
  dbname: "$DB_NAME"
  sslmode: "disable"
EOF

	# save the current registry configuration
	cp  /var/opt/gitlab/registry/config.yml /var/opt/gitlab/registry/database_config.yml
	
	echo "WARNING!! ⚠️⚠️⚠️"
	echo "Running \"gitlab-ctl reconfigure\" will reset the registry configuration. A copy of the configuration is available at \"/var/opt/gitlab/registry/database_config.yml\""
	echo "Make sure to replace the registry configuration with the database configuration if you reconfigure gitlab"
}

run_schema_migrations () {
	# check pending schema migrations
	/opt/gitlab/embedded/bin/registry database migrate status /var/opt/gitlab/registry/config.yml

	prompt_confirm "Run schema migrations?" || restore_all_files

	/opt/gitlab/embedded/bin/registry database migrate up /var/opt/gitlab/registry/config.yml

	echo "Verify the output of the schema migrations"
}


run_all() {
	run_as_root
	prepare_gitlab_configuration
	configure_registry_settings
	run_schema_migrations

	prompt_confirm "Start registry with metadata database enabled?" || restore_all_files

	gitlab-ctl start registry

	echo "Verify that the registry has started with the database enabled. E.g. run 
	gitlab-ctl tail registry | grep metadata
	and look for the string \"level=warning msg=\"the metadata database is an experimental feature, please do not enable it in production\"\""
}
